# Polytheist greaterplayer in 28 wins out of 933 games (the fewest possible wins)

Just go read [the
post](https://antalsz.gitlab.io/dcss-polytheist-greaterplayer-blog-post/minimal-polytheist-greaterplayer.html),
or check out everything in the [`public/`](public/) folder.
