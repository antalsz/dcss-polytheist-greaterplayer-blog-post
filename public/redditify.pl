#!/usr/bin/env perl
use strict;
use warnings;

my $printed = 1;
while (<>) {
  my $skip_blank = not $printed;
  $printed = 0;
  
  # Simple superscripts
  s/(?<!\\)(\^[^\^\s]+)\^/$1/g;
  
  # Username pings for a couple folks (deleting the corresponding links)
  my $users = qr/Oneirical|Sastreii/;
  next if /^\[$users\]: /;
  s{\[(?<user>$users)\]|(?<!\[)(?<user>$users)}{/u/\l$+{user}}g;
  
  # Strip YAML block and replace it with the Reddit title as a comment
  if (/^---$/.../^---$/) {
    if (/^title: +(.+)/) {
      $_ = "<!-- [YAVP]×28: $1 -->\n\n";
    } else {
      next;
    }
  }
  
  # Reddit/standalone toggle
  next if m{^\[(/r/dcss|Reddit version)\]: };
  next if (m{<reddit>}..m{</reddit>}) && m{</?reddit>};
  next if m{<standalone>}..m{</standalone>};
  s{\[/r/dcss\]}{this subreddit}g;
  s{^(\[[^\]]+\]: +)((?:images|pdfs)/)}{$1http://singleton-dcss-polytheist-greaterplayer.antalsz.com/$2};
  
  # Output
  next if $skip_blank && /^$/;
  print;
  $printed = 1;
}
