<!-- [YAVP]×28: Polytheist greaterplayer in 28 wins out of 933 games (the fewest possible wins) -->

I achieved polytheist greaterplayer status in 933 games, taking the minimal
number of wins possible (28) by never repeating a background, god, or species
unless necessary!  You can check out my [score page] and [stats page] at the
usual places; alternatively (especially if you’re reading this from the future),
you can check out my [grid][first win grid] or [list][win list] of wins, also
available in a [score PDF] or [stats PDF] if you’d rather.  (Why did this take
28 wins and not 27, you ask?  0.30, with Armataurs and Reavers, came out while I
was playing 0.29, but I’d already won a Palentonga Abyssal Knight!)

You can also find this post
at
<http://singleton-dcss-polytheist-greaterplayer.antalsz.com/minimal-polytheist-greaterplayer.html>,
in preparation for the upcoming subreddit blackout
protest action (solidarity!).

I started playing DCSS [just about two years ago (July 31,
2021)](https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20210731-081203.txt),
although I’d played [a bunch of
NetHack](https://nethackscoreboard.org/players/a/antalsz.nh.html) and some
Brogue before.  I insisted on [my first win][DEWz\^Sif] being a DEWz\^Sif,
because dammit, I wanted to win as a caster, and those were the three casteriest
choices!  (My first NetHack ascension was a Wiz-Elf-Mal-Cha, back in the day.)
In retrospect: wow, what a choice.  It took me 109 games of trying over a month
and a half, but I got that first win!

I then decided I wanted to try something totally different, so I went with a
FoFi\^Qaz – I loved the sound of a shield plus an executioner’s axe.  Again, in
retrospect: a Formicid of Qazlal‽  But it only took me 4 games over 4 days to
get [my second win][FoFi\^Qaz].  (Only had a battleaxe, in the end.)

Around here I committed to the idea of going for polytheist greaterplayer in the
minimal number of games, and started to work out my list of characters.  One
thing I decided on early was that I was going to delay the canonical “easy
characters” for new players as late as possible, to… well, to show off, I guess,
let’s be honest.  So I put DrCj\^Veh and MiBe\^Trog at 24 and 25 on my list,
respectively.  Other than that, I let myself play what I wanted, repeating
characters until I won, and occasionally tweaking the list.  I played on stable,
since the thought of a game breaking out from under me on trunk was unappealing.
By sheer chance, my very first game from above was the *day after* the release
of 0.27; it took me until after the release of 0.30 to complete my goal.

I *really* like DCSS.  I absolutely love the spellcasting; the different spells
feel interesting to cast, and meaningfully different from each other.  Casting
[Maxwell’s Capacitive Coupling] is different from [Shatter] is different from
[Polar Vortex] – or, at lower levels, [Foxfire] is different from [Magic Dart]
is different from [Freeze].  The weapon abilities, armor and weapon brands, and
random and fixed artifacts all come together for a really dynamic equipment
game, too.  I think the skilling system is neat but really hard to get a handle
on; there’s a high skill ceiling there (no pun intended).  On a more meta-level,
I really appreciate the [philosophy] of Crawl, particularly around a lack of
grinding.  The only downside is how much the game fills my head even when I’m
not playing; it’s fun, but potentially a little bit all-consuming.

A couple of spell opinions: if I find [Olgreb’s Toxic Radiance] early, I’ll try
to retool almost any character who plausibly could to cast it; it nukes
everything, and means I can hit Orc before Lair.  Plus, it trivializes (the
offensive part of) Spider and to an extent Shoals.  And that’s all *without*
adding in [Ignite Poison]!  Mid- to late-game, I think [Yara’s Violent
Unravelling] is one of the strongest spells available.  It cleans up against
summoners, so no sudden [Tzitzimimeh][Tzitzimitl] or [Brimstone
Fiends][Brimstone Fiend] or [Neqoxecs][Neqoxec] from your neighborhood
demonologists; it dispels scary buffs like haste and might; and it cancels
dangerous spells like, in particular, [ironbound convokers’][ironbound convoker]
[Word of Recall].  *And* it does all this by turning enemies into bombs!  What’s
not to love?  It’s worth going out of your way for, IMO.

I definitely (as one would hope!) improved over the course of this journey.  I
found myself regularly going for 4 or 5 runes (I got much less scared of Abyss
over time, though I still default to Vaults:5 or Slime for my 3rd) and the
Vestibule of Hell, though that’s partly because I always like getting stronger
characters and seeing what they can do.  I learned that I should refine my
skilling down to my actual goals, and sometimes even managed to actually do
that.  By the same token, I’ve at least started to learn to embrace using my
consumables.  And I just got better at tactics; some of these I got to bring in
from NetHack, but others were different (e.g., NetHack’s doors restrict you to
4-way movement, and you can only shoot in the 8 cardinal directions).

What’s next for me with DCSS?  Well, I’ll maybe take a bit of a break, but my
[official score page][score page] doesn’t know that Arcane Marksmen were renamed
to Hexslingers, so it looks like I’m missing that… and it has an empty column
for skald for some reason, so maybe I should fill that in… and while I’m back
there, I never did play a deep dwarf or a centaur or… oh hey, the [stats page]
records that I’ve only won 1 draconian color towards Tiamat….  Or I suppose I
could just play some games for fun, but hey, where’s the fun in that? :-)

Major thanks to this subreddit, [the wiki][Crawl Wiki] (*so* glad it’s back!), the
[learndb] and the [knowledge bots], [listgame] and Sequell, and the #crawl IRC
channel for being incredibly valuable resources for learning about DCSS.  And my
profound gratitude to the Devteam for putting together an incredible game and
continuing the development [in the open][Crawl GitHub] (I lurk #crawl-dev
sometimes), and Linley Henzell for writing the original Crawl back in the day.

Here are a few thoughts on my 933-game, 28-win journey, character by character,
for anybody who’s interested in some of the details.

1.  [DEWz\^Sif], 3 runes *(109 games)*.  So much floundering, so many bad branch
    orders, so much confusion about skilling!  I was very much learning the game
    during this – at one point, I said “I think my short sword with rF+ and MP+
    is better than a +6 quick blade”.  Still, despite everything, I got
    [Shatter] castable in my winning game, and that can’t have hurt :-)

2.  [FoFi\^Qaz], 4 runes *(4 games)*.  Things improved pretty fast!  This only
    took me four tries, and I snagged both the silver (for the first time) and
    slimy runes.

3.  [GhEE\^Yred], 3 runes *(46 games)*.  As of this writing, I hold the only
    online win for a GhEE\^Yred, and I see why.  I thought, oh, ghouls have okay
    earth aptitudes, undead fit with Yredelemnul… nope.  It took me until
    playing demigods (my 27th win) to *really* understand how important stats
    are.

4.  [SpEn\^Wu], 4 runes *(30 games)*.  This was *fun*!  Dart, dart, lunge, stab,
    kaboom!  After losing two games here, I put DCSS down for almost a year, and
    came back to 0.29 (having skipped 0.28 entirely).  It also featured my one
    departure from my rigid plan – I found [an early Jiyva altar in a Sewer
    once](https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20220930-032518.txt),
    and decided to roll with it (before dying).

5.  [PaAK\^Lu], 5 runes *(4 games)*.  Knowing that Palentongas and Abyssal
    Knights were being removed, I wanted to get a game in with them.
    Conveniently, I’d already planned on playing a PaAK.  It was so different –
    I’d gotten so used to running away!

6.  [FeSu\^~~Onei~~Gozag>Jiyva], 15 runes *(2 games)*.  I saw /u/oneirical’s
    [guide][Oneirical 3-rune]/[s][Oneirical 15-rune] and was just so tempted, so
    I rejiggered my list to fit it in.  What a blast!  The guide was good enough
    that this only took me *two tries*, and that was for my very first time in
    extended (beyond the Vestibule of Hell), a ziggurat (which I completed!) or
    even the Crypt!  Thanks, /u/oneirical!

7.  [OgGl\^Oka], 3 runes *(137 games)*.  I don’t know why I found this so hard –
    harder even than my DEWz when learning the game!  The thinking was: go
    Okawaru, get a great artifact giant spiked club from them, get a bunch of
    large rocks from them, Hulk smash.  I didn’t get that GSC, but I did smash
    things with a great mace they gave me and a GSC that I enchanted myself.

8.  [NaVM\^Chei], 4 runes *(10 games)*.  [Olgreb’s Toxic Radiance] my beloved.
    I found the [woodcutter’s axe] on D:5 in the hands of a gnoll bouda in a
    vault, and it almost killed me
    (`6673 | D:5      | HP: 3/70 [gnoll bouda/the +3 woodcutter's axe {vorpal} (1)]`),
    but I used it for the rest of the game – it’s an awesome find on a
    spellcaster, I never put a single skill point into Axes.

9.  [HOMo\^Beogh], 4 runes *(27 games)*.  My first time playing skill-target
    games to get a title – I wanted to be a Messiah, so I targeted Fighting at
    26.9 while letting Invocations hit 27 (and dipped into a ziggurat to get my
    piety back up after Z:5).

10. [DjFE\^Makh], 15 runes *(41 games)*.  I really like Djinn.  You can cast *so
    much*!  I had the [Elemental Staff], which was great fun; [Iron Shot] +
    [Chain Lightning] is a great pair of spells, too.  After I realized how much
    I could cast, I made it a point not to make any physical attacks – in the
    end, I only made *three* mêlée attacks all game, and they were all
    accidental (and no throwing at all).  This character just felt super strong,
    so after finishing Zot I decided to take my first unguided journey into
    extended, and ended up completing *two* ziggurats after getting all my
    runes.  ([Lom Lobon] was scary, though – I had multiple close calls and used
    a *ton* of scrolls of blinking.)

11. [VSWr\^Usk], 8 runes *(43 games)*.  I’m proud of this game for being one
    where (1) I realized I could go into extended, but (2) I realized that
    discretion was the better part of valor and left early after exiting a
    ziggurat with single-digit HP from a mummy floor.  I used an artifact spear
    of speed all game because I found it early and it was fast enough to
    capitalize on my bite, but I think that was a mistake and I should’ve gone
    to/back to short blades to get the stabbing bonus (never found a quick
    blade, though).  On the upside, I found a potion of experience in a bailey
    in my winning game, only to discover I was *already* carrying *two others*!
    I made the tactical error of Grand Finale-ing [the Royal Jelly][TRJ], and
    then almost dying to engulfment by quicksilver oozes.

12. [TeAE\^Zin], 5 runes *(5 games)*.  Since I was worshiping Zin I wanted to go
    to Hell, but this character didn’t feel strong enough.  Otherwise, not
    terribly remarkable.  I did pick up [Wyrmbane] in Depths, which was a good
    time, as I was barely doing any physical combat before that.

13. [BaAM\^Hep], 5 runes *(1 game!)*.  My only one-and-won and consequently (due
    to my play-one-character-repeatedly approach) my only streak!  Barachi,
    Hexslinger (formerly Arcane Marksman), and Hepliaklqana all seem to be
    really strong together.  And it didn’t hurt that I bought [Zephyr] in a shop
    in Snake; that thing is *nuts*.  I did a little bit of a ziggurat before
    Zot:5, but decided to take the 5-rune win rather than get cocky.

14. [MfIE\^TSO], 15 runes *(43 games)*.  At first, a Mf\^TSO was where I was
    planning to do extended for the first (and only?) time; thanks to /u/oneirical,
    I learned I needn’t be so scared.  Still, I decided I was going to take this
    character all the way; I wanted to play with one of TSO’s holy weapons!  So…
    [I died to Gloorx Vloq due to lethal
    poison](https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230224-060629.txt).
    But eventually I made it!  I got to use both a eudemon blade from The
    Shining One and [the trishula “Condemnation”], so really taking advantage of
    all the Merfolk aptitudes.  I even cleared a ziggurat!  Other highlights
    include accidentally clearing Slime without rCorr (it was only on an
    artifact weapon, and I forgot I wasn’t using it until after dispatching TRJ)
    and realizing in a ziggurat that a potion of attraction + Cleansing Flame
    can actually *recover* you a lot of health as a worshiper of TSO against
    smiters/damnationers/tormenters who are trying to keep their distance.

15. [MuNe\^Ash], 5^\* runes *(20 games)*.  My winning game got one extended rune
    – icy, from Cocytus (in addition to silver, slimy, barnacled, and
    serpentine).  It didn’t seem to me like Mummies were as hard as people say,
    but that might be because Ashenzari *is* as good as people say.  I had [a
    very frustrating
    death](https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230308-062156.txt)
    where I tried to take advantage of being undead to clear some of Tomb:1 but
    misremembered how far in I could go – and this in a game where I forgot to
    do Vaults:1–4 and went straight to Depths, and cleared it!  My winning game,
    I went back and revenged myself on the safe part of Tomb:1, at least.  I
    also killed [Mennas] and dipped into a ziggurat.  I was surprised how good
    [Haunt] was; I took it as a lesson in the danger of being surrounded.

16. [KoDe\^Dith], 5 runes *(16 games)*.  It’s amazing how good an early stab on
    a red enemy as a Delver feels, as you rocket up in XP.  I hit `#*********`
    stealth before picking up the orb.

17. [HuCA\^Ignis], 3 runes *(23 games)*.  I know it’s not “intended”, but I had
    to keep Ignis for polytheist.  That meant I underused their abilities, since
    I was nervous about running out.  In my winning run, [Wyrmbane] was
    graciously donated by Maggie on D:8, and I dropped axes like a hot potato.
    Obnoxiously, though, the very first gloves to spawn were the [Mad Mage’s
    Maulers] on Shoals:4.

18. [VpBr\^Kiku], 5 runes *(32 games)*.  I definitely preferred to be bloodless;
    the benefits seem really strong.  Not much else to say here.

19. [MeAr\^Ely], 3 runes *(18 games)*.  I was not looking forward to this; I
    play slooooowly.  But it was more fun than I expected!  Elyvilon is *great*,
    and not just in 0.29 for Meteorans where you can use them to get rid of your
    Zot clock drain (a synergy I opted into accidentally).  Smite-targeted “Heal
    Other” to OHKO enemies?  Amazing.  And Meteoran aptitudes are great!  Still,
    not planning to do this again any time soon.  Also I’m kicking myself for
    missing an enchant weapon scroll that would’ve let me bring my lajatang up
    to +9.  How do people take Meteorans into extended?  I felt like I was
    running out of Zot clock so often!

20. [GnWn\^Nem], 5 runes *(6 games)*.  I got to be a hybrid tank casting 5th and
    6th level spells in crystal plate, what’s not to love?  Also vampiric axes
    are incredible.  Nemelex wasn’t too shabby either.  Unfortunately, I really
    wanted to rescue Crazy Yiuf this run, but I wasn’t thinking and killed him
    with [Manifold Assault] trying to get a balrug while *standing on the exit*.

21. [GrCK\^Xom], 10 runes *(48 games)*.  The Xom-meister!  Another run I wasn’t
    looking forward to (I had this and Meteoran classed as “Miserable” in my
    planning document), so I paired it with a powerful species.  I got a bit
    cocky and wanted to try extended runes with Xom due to some great equipment,
    so [of course I died in Pandemonium with 7
    runes](https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230401-012953.txt)
    on game 6 (I hadn’t noticed that I could read scrolls again).  But I did
    eventually win with a ziggurat and the 5 Pan runes; I tried Cocytus, escaped
    a hairy situation, and decided enough was enough with Xom.  Extended runes
    with Xom!  I’m proud of that, but no more Xom for me, thanks.

22. [OpTm\^Gozag], 15 runes *(9 games)*.  After /u/oneirical’s guide, I sort of
    figured this character would do 15 runes.  And boy howdy did it.  (Plus a
    ziggurat!)  [Dragon Form] was strong early, [Storm Form] and [Necromutation]
    were naturally stronger later.  (This might be the run where I forgot how to
    do extended without Necromutation, actually.)  And Bribe Branch is *bonkers*
    strong; Zot:5 and Tomb were so chill.  Bonus: the [Octopode lich tile] is so
    cute!  Thank you, /u/sastreii!

23. [DsHu\^Fedhas], 5 runes *(66 games)*.  Some sloppy play made this take so
    many games.  (What early-me would’ve said to calling this “so many”!)
    Fedhas seems quite good, although in retrospect going ranged with Demonspawn
    means your body mutations feel pretty pointless unless you get [antennae]…
    which I did in my winning game, so that’s nice :-) Overgrow is neat; I was
    able to crack into the Zot lungs, but I sadly didn’t get a Gauntlet so I
    couldn’t sequence-break it.

24. [DrCj\^Veh], 15 runes *(5 games)*.  As I mentioned, I deliberately put off
    the canonical “easy characters” as late as I could.  So here was a (grey)
    DrCj\^Veh, the magic character that, in retrospect, maybe I should’ve
    started with.  (I think renaming “Wizard” to “Hedge Wizard” plus changing
    the abbreviation was a good call.)  Okay, so taking it into extended and
    completing a Ziggurat isn’t a starting-character approach!  [Shatter]
    remains great, but I feel like [Fire Storm] is overrated?  I got to use the
    [Elemental Staff] again, which is always a treat!

25. [MiBe\^Trog], 3 runes *(4 games)*.  Ah, the classic starting character.
    This is as late as I could put it: 25th, the final background.  (After this,
    I had to repeat *something* until the release of 0.30, but that happened
    later.)  What is there to say?  I acquired “the +9 executioner's axe
    "Cusuarph" {vorpal, rC+ Dex+4}” in a Bailey on turn 10129, basically stapled
    it to my hands almost immediately (it hit mindelay mid-Lair), and then threw
    on some crystal plate I found in Elf:3.  Grandplayer complete!

26. [TrGl\^Ru], 4 runes *(19 games)*.  Now I was into repeats!  I found this
    character challenging, but Ru is really strong.  I [lost a
    character](https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230418-003010.txt)
    with the [scales of the Dragon King] and the [shield of the gong] in Slime
    after clearing Vaults, which was more than a bit tragic, but my winning game
    found mundane gold dragon scales on D:7 so it worked out.  I sacrificed
    Arcana (Earth, Hexes, and Necromancy, not that it mattered), Nimbleness,
    Stealth, Artifice, and Drink, which worked out.  Polytheist complete!
    (Unless that includes atheist.)

27. [DgHW], 15 runes *(153 games)*.  I decided I wanted to close out the 0.29
    list with a Hedge Wizard, coming back to where I started.  Then.  That took.
    *Forever*.  The most games of any combo.  0.30 came out while I was working
    on this character!  Admittedly, I
    [lost](https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230503-071543.txt)
    six
    [different](https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230511-004223.txt)
    Demigods
    [in](https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230513-145113.txt)
    extended
    [that](https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230519-232553.txt)
    I
    [could’ve](https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230521-070627.txt)
    taken
    [to](https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230525-025816.txt)
    a 3–5-rune win.  (Fuck Dis, fuck Ziggurats.)  My first extended loss was
    after a much more reasonable 23 games (and I claim partly due to lag – I hit
    `Xkk` to examine a panlord while at lowish health, the game missed the `X`,
    and…  not that I would necessarily have survived anyway, but still).  After
    30 games, the 0.30 tournament started, and although [Call Imp] got [a lot
    better][Call Imp buff], I didn’t get a *single win* [during the
    tourney][0.30 tournament score page] as I kept brute-forcing extended (but
    did get the combo high score, somehow).  I finally won, using [Chain
    Lightning] and *very* aggressive casting of [Maxwell’s Capacitive Coupling].
    I tried to do a ziggurat after Dis but before Tartarus (!) of all places,
    and while I found the [Necromutation] I was looking for, I had to bail.
    After Tomb, I decided I really wanted the ziggurat completion, and tried
    again – I had to run through most of the final floors, but I got out!  And
    finally rescued Crazy Yiuf, to boot!  If you require atheist, then now it
    was polytheist complete!

28. [AtRe\^faded altar Jiyva], 4 runes *(12 games)*.  Now that 0.30 was out, I
    had Armataurs and Reavers to win.  So, like everybody else, I played a bunch
    of AtRes.  I went for a faded altar when I could, since I hadn’t done that
    yet.  My [first
    game](https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230603-184346.txt)?
    *Xom*.  It actually went pretty well, though, despite the eventual death
    (after conversion to Cheibriados).  My eventual win was with a faded altar
    Jiyva from D:3, and while the loss of consumables wasn’t great, you hit max
    piety *fast*.  I definitely underused their abilities, particularly later.
    But I made it out.  And with that: greaterplayer complete!!

If you really did make it all the way down here, thanks for reading everything!
I really like Crawl, but don’t have a ton of folks I know in my daily life who
play/ed it, so it’s nice to share with a community who knows what I’m talking
about :-)

[score page]:                 http://crawl.akrasiac.org/scoring/players/antalsz.html
[stats page]:                 https://dcss-stats.vercel.app/players/antalsz
[0.30 tournament score page]: https://crawl.develz.org/tournament/0.30/players/antalsz.html

[score PDF]: http://singleton-dcss-polytheist-greaterplayer.antalsz.com/pdfs/DCSS%20score%20page%20–%20polytheist%20greaterplayer.pdf
[stats PDF]: http://singleton-dcss-polytheist-greaterplayer.antalsz.com/pdfs/DCSS%20stats%20page%20–%20polytheist%20greaterplayer%20(first%20win).pdf

[first win grid]: http://singleton-dcss-polytheist-greaterplayer.antalsz.com/images/DCSS%20stats%20page%20–%20polytheist%20greaterplayer%20(first%20win)%20grid.png
[win list]:       http://singleton-dcss-polytheist-greaterplayer.antalsz.com/images/DCSS%20score%20page%20–%20polytheist%20greaterplayer%20win%20list.png

[DEWz\^Sif]:                 https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20210914-045951.txt
[FoFi\^Qaz]:                 https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20210918-075103.txt
[GhEE\^Yred]:                https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20210930-035322.txt
[SpEn\^Wu]:                  https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20221010-064831.txt
[PaAK\^Lu]:                  https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20221014-003631.txt
[FeSu\^~~Onei~~Gozag>Jiyva]: https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20221022-231204.txt
[OgGl\^Oka]:                 https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230101-223108.txt
[NaVM\^Chei]:                https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230122-110058.txt
[HOMo\^Beogh]:               https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230125-092858.txt
[DjFE\^Makh]:                https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230202-105710.txt
[VSWr\^Usk]:                 https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230210-074621.txt
[TeAE\^Zin]:                 https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230215-101326.txt
[BaAM\^Hep]:                 https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230218-035343.txt
[MfIE\^TSO]:                 https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230304-203537.txt
[MuNe\^Ash]:                 https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230311-081518.txt
[KoDe\^Dith]:                https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230318-065137.txt
[HuCA\^Ignis]:               https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230322-020656.txt
[VpBr\^Kiku]:                https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230326-223621.txt
[MeAr\^Ely]:                 https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230329-064645.txt
[GnWn\^Nem]:                 https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230331-053559.txt
[GrCK\^Xom]:                 https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230404-045107.txt
[OpTm\^Gozag]:               https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230406-100653.txt
[DsHu\^Fedhas]:              https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230411-071454.txt
[DrCj\^Veh]:                 https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230413-235045.txt
[MiBe\^Trog]:                https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230414-063005.txt
[TrGl\^Ru]:                  https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230422-054322.txt
[DgHW]:                      https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230601-023234.txt
[AtRe\^faded altar Jiyva]:   https://crawl.kelbi.org/crawl/morgue/antalsz/morgue-antalsz-20230605-144220.txt

[Oneirical 3-rune]:  http://crawl.chaosforge.org/Onei%27s_3-rune_Interdimensional_Feline_Kidnapping_Walkthrough_-_FeSu%5EGozag/Jiyva
[Oneirical 15-rune]: http://crawl.chaosforge.org/Onei’s_15-rune_Travel_Guide_on_Thrilling_Ventures_in_the_Demonic_Funhouses_-_FeSu%5EJiyva

[philosophy]: https://github.com/crawl/crawl/blob/master/crawl-ref/docs/crawl_manual.rst#n-philosophy-pas-de-faq

[Crawl Wiki]:     http://crawl.chaosforge.org/Crawl_Wiki
[learndb]:        https://loom.shalott.org/learndb.html
[knowledge bots]: http://crawl.develz.org/info/index.php?q=knowledge+bots
[listgame]:       https://github.com/crawl/sequell/blob/master/docs/listgame.md
[Crawl GitHub]:   https://github.com/crawl/crawl

[woodcutter’s axe]:            http://crawl.chaosforge.org/Woodcutter%27s_axe
[Elemental Staff]:             http://crawl.chaosforge.org/Elemental_Staff
[Wyrmbane]:                    http://crawl.chaosforge.org/Wyrmbane
[Zephyr]:                      http://crawl.chaosforge.org/Zephyr
[the trishula “Condemnation”]: http://crawl.chaosforge.org/Condemnation
[Mad Mage’s Maulers]:          http://crawl.chaosforge.org/Mad_Mage%27s_Maulers
[scales of the Dragon King]:   http://crawl.chaosforge.org/Scales_of_the_Dragon_King
[shield of the gong]:          http://crawl.chaosforge.org/Shield_of_the_gong

[Maxwell’s Capacitive Coupling]: http://crawl.chaosforge.org/Maxwell%27s_Capacitive_Coupling
[Shatter]:                       http://crawl.chaosforge.org/Shatter
[Polar Vortex]:                  http://crawl.chaosforge.org/Polar_Vortex
[Foxfire]:                       http://crawl.chaosforge.org/Foxfire
[Magic Dart]:                    http://crawl.chaosforge.org/Magic_Dart
[Freeze]:                        http://crawl.chaosforge.org/Freeze
[Olgreb’s Toxic Radiance]:       http://crawl.chaosforge.org/Olgreb%27s_Toxic_Radiance
[Ignite Poison]:                 http://crawl.chaosforge.org/Ignite_Poison
[Yara’s Violent Unravelling]:    http://crawl.chaosforge.org/Yara%27s_Violent_Unravelling
[Iron Shot]:                     http://crawl.chaosforge.org/Iron_Shot
[Chain Lightning]:               http://crawl.chaosforge.org/Chain_Lightning
[Haunt]:                         http://crawl.chaosforge.org/Haunt
[Manifold Assault]:              http://crawl.chaosforge.org/Manifold_Assault
[Dragon Form]:                   http://crawl.chaosforge.org/Dragon_Form
[Storm Form]:                    http://crawl.chaosforge.org/Storm_Form
[Necromutation]:                 http://crawl.chaosforge.org/Necromutation
[Fire Storm]:                    http://crawl.chaosforge.org/Fire_Storm
[Call Imp]:                      http://crawl.chaosforge.org/Call_Imp

[antennae]: http://crawl.chaosforge.org/Antennae

[Tzitzimitl]:         http://crawl.chaosforge.org/Tzitzimitl
[Brimstone Fiend]:    http://crawl.chaosforge.org/Brimstone_Fiend
[Neqoxec]:            http://crawl.chaosforge.org/Neqoxec
[ironbound convoker]: http://crawl.chaosforge.org/http://crawl.chaosforge.org/Ironbound_convoker
[TRJ]:                http://crawl.chaosforge.org/The_Royal_Jelly
[Lom Lobon]:          http://crawl.chaosforge.org/Lom_Lobon
[Mennas]:             http://crawl.chaosforge.org/Mennas

[Word of Recall]: http://crawl.chaosforge.org/Word_of_Recall

[octopode lich tile]: https://raw.githubusercontent.com/crawl/crawl/0.30.0/crawl-ref/source/rltiles/player/transform/lich_form_octopode.png
[Call Imp buff]:      https://github.com/crawl/crawl/commit/25214051b3074dfcf722a6c020f209bab081a794
